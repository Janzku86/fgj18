﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public Transform target;
	public Explosion explosion;
	public GameObject graphics;
	public float speed;
	public float damage;
	public float radius;
	public float destructionTime = 3.0f;
	public float explosionDistance = 0.3f;

	void Awake()
	{

	}
	// Use this for initialization
	void Start () 
	{
		AudioManager.PlayAudioClipAtPosition (AudioClipEnum.Effect_Weapons_ProjectileShot, transform.position);
		//gameObject.transform.LookAt (target);
		transform.right = target.position - transform.position;
		StartCoroutine (SelfDestruct());
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.localPosition += transform.right * speed * Time.deltaTime;
		if (Vector3.Distance(transform.position, target.position) < explosionDistance) 
		{
			Detonate ();
		}
	}

	public IEnumerator SelfDestruct()
	{
		yield return new WaitForSeconds(destructionTime);
		Detonate ();
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Detonate();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Sensor") 
		{
			graphics.SetActive (true);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Sensor") 
		{
			graphics.SetActive (false);
		}
	}

	public void Detonate()
	{
		Explosion e = Instantiate (explosion, transform.position, Quaternion.identity);
		e.damage = damage;
		e.radius = radius;
		Destroy (gameObject);
		Destroy (target.gameObject);
	}
}
