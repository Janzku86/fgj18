﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour {

	public float rotX = 0;
	public float rotY = 0;
	public float rotZ = 0;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		{
			gameObject.transform.Rotate (rotX,rotY,rotZ);
		}
	}
}
