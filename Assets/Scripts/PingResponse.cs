﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingResponse : MonoBehaviour 
{
	public TextMesh textMesh = null;
	public SpriteRenderer spriteRenderer = null;


	public void SetAlpha(float alpha)
	{
		if (textMesh != null)
		{			
			Color color = textMesh.color;
			color.a = alpha;
			textMesh.color = color;
		}
		if (spriteRenderer != null) 
		{
			Color color = spriteRenderer.color;
			color.a = alpha;
			spriteRenderer.color = color;
		}
	}
}
