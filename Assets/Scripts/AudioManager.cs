﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public enum AudioClipEnum
{
	none = -1,

	Ambient_DeepSpaceSilence = 0,
	Ambient_SpaceEngineHum = 1,
	Ambient_SpaceWhoosh = 2,

	Effect_ComputerBeep1 = 10,
	Effect_ComputerBeep2 = 11,
	Effect_RadioStatic1 = 12,
	Effect_RadioStatic2 = 13,
	Effect_Frog = 14,
	Effect_Explosion1 = 15,
	Effect_Explosion2 = 16,
	Effect_Explosion3 = 17,
	Effect_Explosions = 18,

	Effect_Sonar_Ping = 19,
	Effect_Sonar_BongSmall = 20,
	Effect_Sonar_BongNeutral = 21,
	Effect_Sonar_BongBig = 22,

	Effect_Weapons_LaserHit1 = 25,
	Effect_Weapons_LaserHit2 = 26,
	Effect_Weapons_LaserHit3 = 27,
	Effect_Weapons_LaserHits = 28,
	Effect_Weapons_LaserShots = 29,

	Effect_Weapons_ProjectileHit = 30,
	Effect_Weapons_ProjectileShot = 31,


	VoiceLine_Casual = 40,
	VoiceLine_Evade = 41,
	VoiceLine_IncomingHostiles = 42,
	VoiceLine_InitiatingTarget = 43,
	VoiceLine_TargetDestroyed = 44,
	VoiceLine_Radio_ResumingPatrol = 45,
	VoiceLine_Radio_TargetLostAngry = 46,
	VoiceLine_Radio_TargetLostNeutral = 47,
	VoiceLine_UnderAttack = 48,
	VoiceLine_UnknownObjectDetected = 49,
	VoiceLine_WeAreUnderAttack = 50,
	VoiceLine_Perkele = 51,
	VoiceLine_Annoying = 52,
}

[System.Serializable]
public class AudioEnumClip
{
	public AudioClipEnum EnumID;
	public AudioClip AudioClip;
}

/// <summary>
/// A class to contain all AudioClips.
/// </summary>
public class AudioManager : MonoBehaviour 
{
	static AudioManager m_instance;

	[SerializeField]
	AudioSource m_audioSource;

	[SerializeField]
	List<AudioEnumClip> m_audioClips;

	private Dictionary<AudioClipEnum, AudioClip> m_audioClipsByEnum = null;

	private List<AudioSource> m_audioSources = null;

	// Use this for initialization
	void Start () 
	{
		m_instance = this;

		if (m_audioClipsByEnum == null) 
		{
			m_audioClipsByEnum = new Dictionary<AudioClipEnum, AudioClip> ();

			for (int i = 0; i < m_audioClips.Count; i++) 
			{
				if (m_audioClipsByEnum.ContainsKey (m_audioClips [i].EnumID)) 
				{
					Debug.LogError ("Audioclip dictionary alreayd contains [" + m_audioClips [i].EnumID + "]");
					continue;
				}

				m_audioClipsByEnum.Add (m_audioClips [i].EnumID, m_audioClips [i].AudioClip);
			}
		}
	}

	public static AudioClip GetAudioClip(AudioClipEnum clipEnum)
	{
		if (m_instance == null) 
		{
			Debug.LogError ("Audio Manager has not bee initialized! Make sure there is AudioManager in scene!");
			return null;
		}

		if (m_instance.m_audioClipsByEnum.ContainsKey (clipEnum)) 
		{
			return m_instance.m_audioClipsByEnum[clipEnum];
		}

		Debug.LogWarning ("AudioClip dicitonary did not contain key [" + clipEnum + "]");
		return null;
	}

	public static bool PlayAudioClipAtPosition(AudioClipEnum clipEnum, Vector3 position, bool loop = false)
	{
		if (m_instance == null) 
		{
			Debug.LogError ("Audio Manager has not bee initialized! Make sure there is AudioManager in scene!");
			return false;
		}

		if (m_instance.m_audioSource.isPlaying) 
		{
			if (m_instance.m_audioSources == null)
				m_instance.m_audioSources = new List<AudioSource> ();

			for (int i = 0; i < m_instance.m_audioSources.Count; i++) 
			{
				if (m_instance.m_audioSources [i].isPlaying == false) 
				{
					m_instance.m_audioSources [i].gameObject.transform.position = position;
					return PlayAudioClip (clipEnum, m_instance.m_audioSources[i], loop);
				}
			}

			m_instance.m_audioSource.clip = null;
			m_instance.m_audioSource.loop = false;
			GameObject go = Instantiate (m_instance.m_audioSource.gameObject, m_instance.transform);
			AudioSource _source = go.GetComponent<AudioSource> ();
			m_instance.m_audioSources.Add (_source);
			_source.gameObject.transform.position = position;
			PlayAudioClip (clipEnum, _source, loop);
		} 
		else 
		{
			m_instance.m_audioSource.transform.position = position;
			return PlayAudioClip (clipEnum, m_instance.m_audioSource, loop);
		}

		return false;
	}

	public static bool PlayAudioClip(AudioClipEnum clipEnum, AudioSource audioSource, bool loop = false)
	{
		if (m_instance == null) 
		{
			Debug.LogError ("Audio Manager has not bee initialized! Make sure there is AudioManager in scene!");
			return false;
		}

		if (audioSource == false) 
		{
			Debug.LogWarning ("AudioSource was null!");
			return false;
		}

		if (m_instance.m_audioClipsByEnum.ContainsKey (clipEnum)) 
		{
			audioSource.Stop ();
			audioSource.loop = loop;
			audioSource.clip = m_instance.m_audioClipsByEnum [clipEnum];
			audioSource.Play ();
			return true;
		}

		Debug.LogWarning ("AudioClip dicitonary did not contain key [" + clipEnum + "]");
		return false;
	}

	public static bool StopAudioClip(AudioSource audioSource)
	{
		if (audioSource == null)
			return false;
		
		audioSource.Stop ();
		return true;
	}

	public static bool StopAudioClip(AudioClipEnum clipEnum)
	{
		if (m_instance == null) 
		{
			Debug.LogError ("Audio Manager has not bee initialized! Make sure there is AudioManager in scene!");
			return false;
		}

		AudioClip _clip = GetAudioClip (clipEnum);

		if (m_instance.m_audioSource.isPlaying && m_instance.m_audioSource.clip == _clip) 
		{
			return StopAudioClip (m_instance.m_audioSource);
		}

		for (int i = 0; i < m_instance.m_audioSources.Count; i++) 
		{
			if (m_instance.m_audioSources [i].isPlaying && m_instance.m_audioSources [i].clip == _clip) 
			{
				return StopAudioClip (m_instance.m_audioSources [i]);
			}
		}

		return false;
	}

	public static void StopAllAudioClips()
	{
		if (m_instance == null) 
		{
			Debug.LogError ("Audio Manager has not bee initialized! Make sure there is AudioManager in scene!");
			return;
		}

		m_instance.m_audioSource.Stop ();
		for (int i = 0; i < m_instance.m_audioSources.Count; i++)
			m_instance.m_audioSources [i].Stop ();
	}
}
