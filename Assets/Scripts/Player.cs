﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour {

	public float acceleration = 0;
	public float speed = 0;
	public float hullIntegrity = 50;
	public float forwardSpeed;
	public float speedMultiplier = 0.001f;
	public float turnRate = 10.0f;
	public GameObject radarPing;
	public GameObject pingResponse;
	public Projectile projectile;
	public GameObject target;
	public bool pingReady = true;
	public float radarCooldown = 0.5f;
	public AudioSource pingSound;
	public AudioSource engineSound;

	Vector3 screenToWorld = new Vector3();
	//public float throttle = 0;

	private List<AudioClipEnum> m_hitEnumsToRandomize = null;

	Rigidbody2D rb;
	// Use this for initialization
	void Start () 
	{
		if (m_hitEnumsToRandomize == null) 
		{
			m_hitEnumsToRandomize = new List<AudioClipEnum> ();
			m_hitEnumsToRandomize.Add (AudioClipEnum.Effect_Weapons_LaserHit1);
			m_hitEnumsToRandomize.Add (AudioClipEnum.Effect_Weapons_LaserHit2);
			m_hitEnumsToRandomize.Add (AudioClipEnum.Effect_Weapons_LaserHit3);
		}

		rb = gameObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*
		if (Input.GetAxis("Vertical") > 0)  
		{
			//throttle += Time.deltaTime *20;
		}
		if (Input.GetAxis("Vertical") < 0) 
		{
			//throttle -= Time.deltaTime *20;
		}
		*/

		if (Input.GetAxis("Horizontal") > 0) 
		{
			//rb.AddTorque (-turnRate);
			gameObject.transform.Rotate(0,0,-turnRate);
		}
		if (Input.GetAxis("Horizontal") < 0) 
		{
			//rb.AddTorque (turnRate);
			gameObject.transform.Rotate(0,0,turnRate);
		}

		if (hullIntegrity <= 0) 
		{
			ShipDestroy ();
		}
		//if (Input.GetMouseButton)
	}

	void FixedUpdate()
	{
		//rb.AddForce (transform.up * speedMultiplier * throttle);
		forwardSpeed = rb.velocity.magnitude;

		if (Input.GetAxis ("Vertical") > 0) 
		{
			if (rb.velocity.magnitude < speed) 
			{
				rb.AddForce (transform.up * acceleration * speedMultiplier);
			}

			if (!engineSound.isPlaying)
				engineSound.Play ();
		} 
		else if (Input.GetAxis ("Vertical") < 0) 
		{
			if (rb.velocity.magnitude < speed) 
			{
				rb.AddForce (transform.up * -acceleration * speedMultiplier);
			}

			if (!engineSound.isPlaying)
				engineSound.Play ();
		} 
		else 
		{
			if (engineSound.isPlaying)
				engineSound.Stop ();
		}
		if (Input.GetKey(KeyCode.LeftControl))
		{
			rb.drag = 1;
			rb.angularDrag = 1;
		}
		else
		{
			rb.drag = 0;
			rb.angularDrag = 0.05f;
		}
		if (Input.GetKey(KeyCode.Space) && pingReady)
		{
			StartCoroutine (StartRadar());
		}

		if (rb.velocity.magnitude > speed) 
		{
			rb.AddForce (-rb.velocity);
		}
		if (Input.GetMouseButtonDown (0)) 
		{
			GameObject t = Instantiate (target, screenToWorld, Quaternion.identity);
			Projectile p = Instantiate (projectile, transform.position, Quaternion.identity);
			p.target = t.transform;
		}
	}

	public IEnumerator StartRadar()
	{
		pingReady = false;
		Instantiate (radarPing, transform.position,Quaternion.identity);
		playPingSound ();
		yield return new WaitForSeconds(radarCooldown);
		pingReady = true;
	}

	private void playPingSound()
	{
		pingSound.pitch = UnityEngine.Random.Range (0.9f, 1.1f);
		pingSound.Play ();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "RadarInbound") 
		{
			GameManager.SpawnPingResponse (this, other);
			//GameObject newResponse = Instantiate (pingResponse, other.gameObject.transform.position, Quaternion.identity);
			//RadarPing replyStats = other.gameObject.GetComponent<RadarPing>();
			//TextMesh replyText = newResponse.GetComponentInChildren<TextMesh>();
			//replyText.text = replyStats.hitID + "\n" + replyStats.hitTime.Hour + ":" + replyStats.hitTime.Minute + ":" + replyStats.hitTime.Second + "\n" + replyStats.hitSize + "\n" + replyStats.hitType;
			//Destroy(other.gameObject);
			//Debug.Log("Pong");
		}
		//if (other.gameObject.tag == "") // Unknown source for vague direction?
		//{
		//}
		//if (other.gameObject.tag == "Explosion") 
		//{
		//	hullIntegrity -= other.gameObject.GetComponent<Explosion> ().damage;
		//
		//	Vector3 direction = (other.transform.position - transform.position).normalized;
		//	Vector3 clipPosition = transform.position + direction * 0.5f;
		//	AudioManager.PlayAudioClipAtPosition (m_hitEnumsToRandomize[UnityEngine.Random.Range(0, m_hitEnumsToRandomize.Count)], clipPosition);
		//}
	}
	void OnGUI()
	{
		Camera  c = Camera.main;
		Event   e = Event.current;
		Vector2 mousePos = new Vector2();

		// Get the mouse position from Event.
		// Note that the y position from Event is inverted.
		mousePos.x = e.mousePosition.x;
		mousePos.y = c.pixelHeight - e.mousePosition.y;

		screenToWorld = c.ScreenToWorldPoint (new Vector3 (mousePos.x, mousePos.y, 1));

		//GUILayout.BeginArea(new Rect(20, 20, 250, 120));
		//GUILayout.Label("Screen pixels: " + c.pixelWidth + ":" + c.pixelHeight);
		//GUILayout.Label("Mouse position: " + mousePos);
		//GUILayout.Label("World position: " + screenToWorld.ToString("F3"));
		//GUILayout.EndArea();
	}
	void ShipDestroy()
	{
		if (Camera.main.GetComponent<AudioListener>() != null)
			Camera.main.GetComponent<AudioListener> ().enabled = true;

		AudioManager.PlayAudioClipAtPosition (AudioClipEnum.Effect_Explosions, Camera.main.transform.position);
		Destroy (gameObject);
	}
}
