﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
	public float radius = 0.5f;
	public float damage = 10.0f;
	public float explosionDuration = 1.0f;
	public float explosionSpeed = 0.5f;
	//public CircleCollider2D col;
	public AudioClipEnum AudioToPlay = AudioClipEnum.Effect_Explosion1;

	float time;
	public bool update = false;
	// Use this for initialization
	void Start () 
	{
		//col = gameObject.GetComponent<CircleCollider2D> ();

		List<AudioClipEnum> _clips = new List<AudioClipEnum> ()
		{
			AudioClipEnum.Effect_Explosion1,
			AudioClipEnum.Effect_Explosion2,
			AudioClipEnum.Effect_Explosion3
		};
		
		AudioManager.PlayAudioClipAtPosition (_clips[UnityEngine.Random.Range(0, _clips.Count)], transform.position);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log ("Time [" + time + "] Duration [" + explosionDuration + "] Col Radius [" + col.radius + "] Radius [" + radius + "]");
		time += Time.deltaTime;
		transform.localScale += new Vector3 (time*explosionSpeed,time*explosionSpeed,time*explosionSpeed);

		if(time >= explosionDuration || transform.localScale.x >= radius)
		{
			//Debug.LogWarning ("Destroy Explosion");
			Destroy(gameObject);
		}
	}
}
