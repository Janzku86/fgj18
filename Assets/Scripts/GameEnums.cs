﻿public enum VesselSize
{
	none = -1,

	S = 0,
	M = 1,
	L = 2,
}

public enum VesselType
{
	none = -1,

	Cargo = 0,
	Military = 1,
}

public enum VesselID
{
	none = -1,

	Alpha = 0,
	Beta = 1,
	Gamma = 2,
	Delta = 3,
}
	
public enum VesselState
{
	none = -1,

	Deliver = 0,
	Evade = 1,
	Escape = 2,
	Patrol = 3,
	Attack = 4,
}