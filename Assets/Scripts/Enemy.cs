﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public GameObject pingReply;
	public VesselID vesselID = VesselID.none;
	public VesselSize vesselSize = VesselSize.none;
	public VesselType vesselType = VesselType.none;
	public VesselState vesselState = VesselState.none;
	public float speed = 1;
	public float rotationSpeed = 1;
	public float hullIntegrity = 10;
	public float boundX = 8.5f;
	public float boundY = 4.7f;
	public float waypointThreshold = 0.2f;
	public Vector3 currentWaypoint = new Vector3();
	public Vector3 nextWaypoint = new Vector3();
	public GameObject graphics;

	private Quaternion lookRot;
	private Vector3 dir;

	private List<AudioClipEnum> m_hitEnumsToRandomize = null;

	// Use this for initialization
	void Start () 
	{
		if (m_hitEnumsToRandomize == null) 
		{
			m_hitEnumsToRandomize = new List<AudioClipEnum> ();
			m_hitEnumsToRandomize.Add (AudioClipEnum.Effect_Weapons_LaserHit1);
			m_hitEnumsToRandomize.Add (AudioClipEnum.Effect_Weapons_LaserHit2);
			m_hitEnumsToRandomize.Add (AudioClipEnum.Effect_Weapons_LaserHit3);
		}
		switch (vesselState) 
		{
			case VesselState.Deliver:
			MoveToStation ();
			break;

			case VesselState.Patrol:
				RandomWaypoint ();
			break;
		}
		//hullIntergrity = hullIntergrity * vesselSize.;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (vesselState == VesselState.Patrol) 
		{
			if(Vector3.Distance(gameObject.transform.position, currentWaypoint) < waypointThreshold)
			{
				RandomWaypoint();
			}
		}
		if (currentWaypoint != null) {
			dir = currentWaypoint - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
			transform.rotation = Quaternion.Slerp (transform.rotation, q, Time.deltaTime * rotationSpeed);
			//transform.rotation = Quaternion.Slerp (transform.rotation, lookRot, Time.deltaTime * rotationSpeed);
			gameObject.transform.localPosition += transform.right * speed * Time.deltaTime;
		}
		if (hullIntegrity <= 0) 
		{
			ShipDestroy ();
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "RadarOutbound") 
		{	
			GameManager.SpawnRadarPing (this);
			//GameObject reply =  Instantiate (pingReply, gameObject.transform.position, Quaternion.identity);
			//RadarPing replyPing = reply.gameObject.GetComponent<RadarPing>();
			//replyPing.hitID = vesselID;
			//replyPing.hitSize = vesselSize;
			//replyPing.hitType = vesselType;
		}
		if (other.gameObject.tag == "EnemyStation") 
		{
			Dock ();
		}

		if (other.gameObject.tag == "Explosion") 
		{
			AudioManager.PlayAudioClipAtPosition (m_hitEnumsToRandomize[UnityEngine.Random.Range(0, m_hitEnumsToRandomize.Count)], transform.position);

			hullIntegrity -= other.gameObject.GetComponent<Explosion>().damage;
		}

		if (other.gameObject.tag == "Sensor") 
		{
			graphics.SetActive (true);
		}
	}
		

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Sensor") 
		{
			graphics.SetActive (false);
		}
	}

	void RandomWaypoint ()
	{
		currentWaypoint =  new Vector3 (Random.Range(-boundX,boundX),Random.Range(-boundY,boundY),0);
	}

	void MoveToStation ()
	{
		currentWaypoint = GameObject.FindWithTag("EnemyStation").transform.position;
	}

	void Dock()
	{
		//replace with something useful
		Destroy (gameObject);
	}

	void ShipDestroy()
	{
		Destroy (gameObject);
	}

}
