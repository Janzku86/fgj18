﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RadarPing : MonoBehaviour {
	public enum PingType {Outbound, Inbound};

	public PingType pingType;
	public float radarSpeed = 0.1f;
	//public GameManager gameManager;
	public DateTime hitTime;
	public string hitID;
	public string hitSize;
	public string hitType;

	public Enemy enemy;

	public AudioClipEnum ClipToPlayOnHit = AudioClipEnum.none;

	void Awake ()
	{
		hitTime = DateTime.Now;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.localScale = gameObject.transform.localScale + new Vector3 (radarSpeed,radarSpeed,radarSpeed);
	}
}
