﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GameManager : MonoBehaviour 
{
	private static GameManager m_instance;
	public int maxCargoShips = 10;
	public int maxMilitaryShips = 6;
	public GameObject cargoShip; 
	public GameObject militaryShip; 
	public float boundX = 8.5f;
	public float boundY = 4.7f;
	public float cargoInterval;
	public float militaryInterval;
	float cargoTimer;
	float militaryTimer;

	private Dictionary<Enemy, List<PingResponse>> m_vesselPingResponseDictionary;
	private Dictionary<Enemy, string> m_vesselIdentifiedDictionary;
	private Dictionary<VesselID, int> m_radarPingsByVesselID;

	public List <GameObject> cargoShips = new List<GameObject>();
	public List <GameObject> militaryShips = new List<GameObject>();
	public List <Transform> jumpGates = new List<Transform>();

	//private List<RadarPing> m_allRadarPings;
	// Use this for initialization
	void Start () 
	{
		if (m_vesselPingResponseDictionary == null)
			m_vesselPingResponseDictionary = new Dictionary<Enemy, List<PingResponse>> ();
		if (m_vesselIdentifiedDictionary == null)
			m_vesselIdentifiedDictionary = new Dictionary<Enemy, string> ();
		if (m_radarPingsByVesselID == null)
			m_radarPingsByVesselID = new Dictionary<VesselID, int> ();

		m_instance = this;
	}

	void Update()
	{
		cargoTimer += Time.deltaTime;
		militaryTimer += Time.deltaTime;

		if (cargoTimer >= cargoInterval) 
		{
			if (cargoShips.Count < maxCargoShips) 
			{				
				SpawnCargoShip ();
			}
			cargoTimer = 0;
		}

		if (militaryTimer >= militaryInterval) 
		{
			if (militaryShips.Count < maxMilitaryShips) 
			{				
				SpawnMilitaryShip ();
			}
			militaryTimer = 0;
		}
	}


	private static bool gameManagerCheck()
	{
		if (m_instance != null)
			return true;

		Debug.LogError ("Game Manager has not bee initialized! Make sure there is GameManager in scene!");
		return false;
	}

	public static void SpawnRadarPing(Enemy enemy)
	{
		if (!gameManagerCheck ())
			return;

		if (enemy == null) 
		{
			Debug.LogError ("Enemy was null!");
			return;
		}

		GameObject reply =  Instantiate (enemy.pingReply, enemy.gameObject.transform.position, Quaternion.identity);
		RadarPing replyPing = reply.GetComponent<RadarPing>();
		if (replyPing == null) 
		{
			Debug.LogError ("Could not get RadarPing from object [" + reply.name + "]");
			return;
		}

		string _suffix = m_instance.getIDSuffix(enemy);
		replyPing.hitID = enemy.vesselID.ToString () + " " + _suffix;
		replyPing.hitSize = enemy.vesselSize.ToString();
		replyPing.hitType = enemy.vesselType.ToString();

		switch (enemy.vesselSize) 
		{
		case VesselSize.S:
			replyPing.ClipToPlayOnHit = AudioClipEnum.Effect_Sonar_BongSmall;
			break;

		case VesselSize.M:
			replyPing.ClipToPlayOnHit = AudioClipEnum.Effect_Sonar_BongNeutral;
			break;

		case VesselSize.L:
			replyPing.ClipToPlayOnHit = AudioClipEnum.Effect_Sonar_BongBig;
			break;
		}

		replyPing.enemy = enemy;
	}

	public static void SpawnPingResponse(Player player, Collider2D other)
	{
		if (!gameManagerCheck ())
			return;

		GameObject newResponse = Instantiate (player.pingResponse, other.gameObject.transform.position, Quaternion.identity);
		PingResponse pingResponse = newResponse.GetComponent<PingResponse> ();
		RadarPing replyStats = other.gameObject.GetComponent<RadarPing>();

		pingResponse.textMesh.text = replyStats.hitID + "\n" + replyStats.hitTime.Hour + ":" + replyStats.hitTime.Minute + ":" + replyStats.hitTime.Second + "\n" + replyStats.hitSize + "\n" + replyStats.hitType;

		if (replyStats.enemy != null) 
		{
			if (m_instance.m_vesselPingResponseDictionary.ContainsKey (replyStats.enemy)) 
			{
				List<PingResponse> _pingResponses = m_instance.m_vesselPingResponseDictionary [replyStats.enemy];

				if (_pingResponses.Count == 4) 
				{
					Destroy (_pingResponses [0].gameObject);
					_pingResponses.RemoveAt (0);
				}

				_pingResponses.Add (pingResponse);

				int _multiplier = 0;
				for (int i = _pingResponses.Count - 1; i >= 0; i--) 
				{
					_multiplier++;
					_pingResponses [i].SetAlpha ( 1f / _multiplier );

				}
			} 
			else 
			{
				m_instance.m_vesselPingResponseDictionary.Add (replyStats.enemy, new List<PingResponse> (){ pingResponse });
			}
		}

		if (replyStats.ClipToPlayOnHit != AudioClipEnum.none) 
		{
			Vector3 direction = (other.transform.position - player.transform.position).normalized;
			Vector3 clipPosition = player.transform.position + direction * 0.2f;

			AudioManager.PlayAudioClipAtPosition (replyStats.ClipToPlayOnHit, clipPosition);
		}

		Destroy (other.gameObject);
	}

	public static void SpawnDirectionIndicator(Player player, GameObject other)
	{
		
	}

	private string getIDSuffix(Enemy enemy)
	{
		if (m_instance.m_vesselIdentifiedDictionary.ContainsKey (enemy))
			return m_instance.m_vesselIdentifiedDictionary [enemy];

		if (m_instance.m_radarPingsByVesselID.ContainsKey (enemy.vesselID)) 
		{
			m_instance.m_radarPingsByVesselID [enemy.vesselID]++;
		} 
		else 
		{
			m_instance.m_radarPingsByVesselID.Add (enemy.vesselID, 1);
		}

		string _suffix = "";
		int _value = m_instance.m_radarPingsByVesselID [enemy.vesselID];
		if (_value < 10)
			_suffix = "0";

		_suffix += _value;

		if (!m_instance.m_vesselIdentifiedDictionary.ContainsKey (enemy))
			m_instance.m_vesselIdentifiedDictionary.Add (enemy, _suffix);

		return _suffix;
	}

	void SpawnCargoShip()
	{
		Transform currentJumpGate = jumpGates [Random.Range (0, jumpGates.Count - 1)];
		GameObject s = Instantiate (cargoShip, currentJumpGate.position,Quaternion.identity);
		cargoShips.Add (s);
	}

	void SpawnMilitaryShip()
	{
		Transform currentJumpGate = jumpGates [Random.Range (0, jumpGates.Count - 1)];
		GameObject s = Instantiate (militaryShip, currentJumpGate.position,Quaternion.identity);
		militaryShips.Add (s);
	}
}